#include <iostream>
#include <math.h>
#include <iomanip>

using namespace std;

class Example
{
private:
	int a;
	string str = "Hello world!\n";
public:
	Example()
	{
		a = 0;
	}

	Example(int newA)
	{
		a = newA;
	}

	int GetA() 
	{
		return a;
	}

	void SetA(int newA)
	{
		a = newA;
	}

	void Print_Hallow()
	{
		cout << str;
	}
};

class Vector
{
private:
	double x;
	double y;
	double z;
	double l = 0.0;
public:
	Vector(): x(0), y(0), z(0)
	{}
	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}

	void Show()
	{
		cout << x << ' ' << y << ' ' << z << '\n';
	}

	double Length()
	{
		l = sqrt(x * x + y * y + z * z);
		return l;
	}
};



int main()
{
	Vector v(10.0,10.0,10.0);
	cout << fixed << setprecision(2) << v.Length() << '\n';

	Example Str;
	Str.Print_Hallow();

	return 0;
}